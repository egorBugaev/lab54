class WinCalculator {
	constructor(cards, suit, rank) {
		this.cards = cards;
		this.suit = suit;
		this.rankArr = rank;
		this.ranks = this.cards.map(card => card.rank);
	}
	
	searchFlush(array = this.cards) {
		for (let thisSuit of this.suit) {
			let suits = [];
			for (let x = 0; x < array.length; x++) {
				if (array[x].suit === thisSuit) suits.push(array[x]);
			}
			if (suits.length >= 5) {
				this.cards.forEach(card => card.active = '');
				suits.forEach(card => card.active = 'active f');
				return suits;
			}
		}
	}
	
	searchRoyalFlush() {
		if (
			this.ranks.includes('10') &&
			this.ranks.includes('j') &&
			this.ranks.includes('q') &&
			this.ranks.includes('k') &&
			this.ranks.includes('a')
		) {
			const royal = [];
			const royalFlush = [];
			let card1 = this.cards.findIndex(p => p.rank === '10');
			let card2 = this.cards.findIndex(p => p.rank === 'j');
			let card3 = this.cards.findIndex(p => p.rank === 'q');
			let card4 = this.cards.findIndex(p => p.rank === 'k');
			let card5 = this.cards.findIndex(p => p.rank === 'a');
			royal.push(card1, card2, card3, card4, card5);
			for (let card of royal) royalFlush.push(this.cards[card]);
			const suits = royalFlush.map(card => card.suit);
			const isRoyalFlush = suits.every(suit => suit === suits[0]);
			if (isRoyalFlush) {
				this.cards.forEach(card => card.active = '');
				royalFlush.forEach(card => card.active = 'active r');
				return true;
			}
		}
	}
	
	searchCombo() {
		const sameCards = [];
		this.cards.forEach(card => card.active = '');
		for (let i = 0; i < this.cards.length; i++) {
			for (let j = i + 1; j < this.cards.length; j++) {
				if (j === this.cards.length) break;
				if (this.cards[i].rank === this.cards[j].rank) {
					this.cards[i].active = 'active combo';
					this.cards[j].active = 'active combo';
					sameCards.push(this.cards[i], this.cards[j]);
				}
			}
		}
		if (sameCards.length === 2) return 'Пара';
		if (sameCards.length === 4) return 'Две пары';
		if (sameCards.length === 6) {
			let someRanks = sameCards.map(card => card.rank);
			const isThree = someRanks.every(card => card === someRanks[0]);
			if (isThree) return 'Тройка';
			else return 'Две пары';
		}
		if (sameCards.length === 8) return 'Фулл хаус';
		if (sameCards.length === 12) {
			let someRanks = sameCards.map(card => card.rank);
			const isPoker = someRanks.every(card => card === someRanks[0]);
			if (isPoker) return 'Каре';
			else return 'Фулл хаус';
		}
	}
	
	searchStraight(array = this.cards, isStraightFlush = false) {
		const sortArray = [];
		const straightArr = [];
		for (let rank of this.rankArr) {
			array.forEach(card => {
				if (card.rank === rank) {
					sortArray.push(card);
				}
			});
		}
		for (let e = 0; e < sortArray.length; e++) {
			let a = sortArray[e].rank;
			let indexA = this.rankArr.findIndex(p => p === a);
			let indexC;
			if (e > 0) {
				let c = sortArray[e - 1].rank;
				indexC = this.rankArr.findIndex(p => p === c);
			}
			if (e + 1 === sortArray.length) {
				if (indexA - indexC === 1 && e > 0) {
					straightArr.push(sortArray[e]);
				}
				break;
			}
			let b = sortArray[e + 1].rank;
			let indexB = this.rankArr.findIndex(p => p === b);
			if (indexB - indexA === 1) straightArr.push(sortArray[e]);
			else if (indexA - indexC === 1 && e > 0) {
				straightArr.push(sortArray[e]);
				break;
			}
		}
		
		if (straightArr.length >= 5) {
			if(!isStraightFlush) {
				this.cards.forEach(card => card.active = '');
				straightArr.forEach(card => card.active = 'active s');
			}
			return straightArr;
		}
	}
	
	straightFlush() {
		const isStraight = this.searchStraight(this.cards, true);
		let flash = [];
		let isStraightFlush = false;
		if (isStraight) flash = this.searchFlush(isStraight,true);
		if(flash) isStraightFlush = this.searchStraight(flash);
		if(isStraightFlush) {
			this.cards.forEach(card => card.active = '');
			isStraightFlush.forEach(card => card.active = 'active sf');
			return true;
		}
	}
	
	highCard() {
		const sortArray = [];
		const array = [...this.rankArr];
		const a = array.splice(0, 1);
		array.push(a[0]);
		for (let rank of array) {
			this.cards.forEach(card => {
				if (card.rank === rank) {
					sortArray.push(card);
				}
			});
		}
		let high = sortArray[sortArray.length - 1];
		const icons = {
			diams: '♦',
			spades: '♠',
			clubs: '♣',
			hearts: '♥'
		};
		this.cards.forEach(card => card.active = '');
		high.active = 'active a';
		console.log(high);
		return `Старшая карта: ${high.rank.toUpperCase()} ${icons[high.suit]}`;
	}
	
	getBestHand() {
		const highCard = this.highCard();
		const isCombo = this.searchCombo();
		const isStraight = this.searchStraight();
		const isFlush = this.searchFlush();
		const isStraightFlush = this.straightFlush();
		const isRoyalFlush = this.searchRoyalFlush();
		if (isRoyalFlush) return 'Роял Флеш';
		else if (isStraightFlush) return 'Стрит флеш';
		else if (isCombo === 'Каре') return 'Каре';
		else if (isCombo === 'Фулл хаус') return 'Фулл хаус';
		else if (isFlush) return 'Флеш';
		else if (isStraight) return 'Стрит';
		else if (isCombo === 'Тройка') return 'Тройка';
		else if (isCombo === 'Две пары') return 'Две пары';
		else if (isCombo === 'Пара') return 'Пара';
		else if (highCard) return highCard;
		else return 'Неопределено';
		
	}
}

export default WinCalculator;