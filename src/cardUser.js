import React from 'react';

const CardUser = props => {
    let classes = `card rank-${props.rank} ${props.suit} ${props.active}`;
    return (
        <div className={classes}>
            <span className="rank">{props.rank.toUpperCase()}</span>
            <span className="suit">{props.suitIcon}</span>
        </div>
    )
};

export default CardUser;
